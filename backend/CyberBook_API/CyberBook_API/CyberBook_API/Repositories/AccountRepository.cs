﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CyberBook_API.Interfaces;
using CyberBook_API.Models;

namespace CyberBook_API.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        public Account GetAccountByID(int id)
        {
            var acc = new Account();
            var context = new CyberBookingContext();
            return acc = context.Accounts.FirstOrDefault(x => x.Id == id);
        }

        public Account GetAccountByUsername(string username)
        {
            var acc = new Account();
            var context = new CyberBookingContext();
            return acc = context.Accounts.FirstOrDefault(x => x.Username == username);
        }

        public IEnumerable<Account> GetAllAccounts()
        {
            var context = new CyberBookingContext();
            return context.Accounts.ToList();
        }
    }
}
