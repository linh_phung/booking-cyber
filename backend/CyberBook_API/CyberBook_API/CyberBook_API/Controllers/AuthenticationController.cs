﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CyberBook_API.Repositories;
using CyberBook_API.Models;
using CyberBook_API.Interfaces;

namespace CyberBook_API.Controllers
{
    [Route("api/Authentication")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {

        private IAccountRepository accountRepository = new AccountRepository();


        [HttpGet("GetAllAccounts")]
        public IEnumerable<Account> GetAllAccounts()
        {
            return accountRepository.GetAllAccounts();
        }


        [HttpGet("GetAccountByID/{id}")]
        public ActionResult<Account> GetAccountByID(int id)
        {
            var acc = accountRepository.GetAccountByID(id);
            if (acc == null)
            {
                return NotFound();
            }
            return acc;
        }

        [HttpGet("GetAccountByUsername/{username}")]
        public ActionResult<Account> GetAccountByUsername(string username)
        {
            var acc = accountRepository.GetAccountByUsername(username);
            if (acc == null)
            {
                return NotFound();
            }
            return acc;
        }
    }
}
