﻿using CyberBook_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyberBook_API.Interfaces
{
    interface IAccountRepository
    {
        IEnumerable<Account> GetAllAccounts();
        Account GetAccountByID(int id);
        Account GetAccountByUsername(string username);

    }
}
