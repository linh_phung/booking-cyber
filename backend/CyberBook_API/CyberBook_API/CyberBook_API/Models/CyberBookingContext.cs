﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace CyberBook_API.Models
{
    public partial class CyberBookingContext : DbContext
    {
        public CyberBookingContext()
        {
        }

        public CyberBookingContext(DbContextOptions<CyberBookingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Cyber> Cybers { get; set; }
        public virtual DbSet<CyberAccount> CyberAccounts { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<RatingCyber> RatingCybers { get; set; }
        public virtual DbSet<RatingUser> RatingUsers { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        public virtual DbSet<RoomType> RoomTypes { get; set; }
        public virtual DbSet<Slot> Slots { get; set; }
        public virtual DbSet<SlotHardwareConfig> SlotHardwareConfigs { get; set; }
        public virtual DbSet<StatusOrder> StatusOrders { get; set; }
        public virtual DbSet<StatusSlot> StatusSlots { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Database=CyberBooking;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Cyber>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.AddressGg)
                    .HasMaxLength(255)
                    .HasColumnName("AddressGG");

                entity.Property(e => e.BossCyberName).HasMaxLength(255);

                entity.Property(e => e.CyberDescription).HasMaxLength(255);

                entity.Property(e => e.CyberName).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CyberAccount>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CyberId).HasColumnName("CyberID");

                entity.Property(e => e.CyberName).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndAt).HasColumnType("datetime");

                entity.Property(e => e.SlotOrderId)
                    .HasMaxLength(255)
                    .HasColumnName("SlotOrderID");

                entity.Property(e => e.StartAt).HasColumnType("datetime");
            });

            modelBuilder.Entity<RatingCyber>(entity =>
            {
                entity.ToTable("RatingCyber");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CommentContent).HasMaxLength(255);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CyberId).HasColumnName("CyberID");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<RatingUser>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CommentContent).HasMaxLength(255);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CyberId).HasColumnName("CyberID");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UsersId).HasColumnName("UsersID");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.RoleName).HasMaxLength(255);
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CyberId).HasColumnName("CyberID");

                entity.Property(e => e.RoomName).HasMaxLength(255);
            });

            modelBuilder.Entity<RoomType>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.RoomDescription).HasMaxLength(255);

                entity.Property(e => e.RoomTypeName).HasMaxLength(255);
            });

            modelBuilder.Entity<Slot>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.RoomId).HasColumnName("RoomID");

                entity.Property(e => e.SlotDescription).HasMaxLength(255);

                entity.Property(e => e.SlotHardwareConfigId).HasColumnName("SlotHardwareConfigID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");
            });

            modelBuilder.Entity<SlotHardwareConfig>(entity =>
            {
                entity.ToTable("SlotHardwareConfig");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Cpu)
                    .HasMaxLength(255)
                    .HasColumnName("CPU");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Gpu)
                    .HasMaxLength(255)
                    .HasColumnName("GPU");

                entity.Property(e => e.Monitor).HasMaxLength(255);

                entity.Property(e => e.NameHardware).HasMaxLength(255);

                entity.Property(e => e.Ram)
                    .IsRequired()
                    .HasMaxLength(255)
                    .HasColumnName("RAM");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<StatusOrder>(entity =>
            {
                entity.ToTable("StatusOrder");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.StatusOrderDescription).HasMaxLength(255);
            });

            modelBuilder.Entity<StatusSlot>(entity =>
            {
                entity.ToTable("StatusSlot");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.StatusSlotDescription).HasMaxLength(255);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.Bio).HasMaxLength(255);

                entity.Property(e => e.Dob).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.Fullname).HasMaxLength(255);

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
