﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CyberBook_API.Models
{
    public partial class Cyber
    {
        public int Id { get; set; }
        public string CyberName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string CyberDescription { get; set; }
        public int? RatingPoint { get; set; }
        public string AddressGg { get; set; }
        public string BossCyberName { get; set; }
    }
}
